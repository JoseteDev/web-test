from behave.__main__ import main as run_behave
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.expected_conditions import visibility_of_element_located
from webdriver_manager.chrome import ChromeDriverManager
import os


api_url = os.environ.get('WEB_URL', 'http://localhost:5011')
photo_counter = 0


def create_driver():
    driver_path = ChromeDriverManager().install()

    chrome_options = Options()
    chrome_options.add_argument('start-maximized')
    chrome_options.add_argument('enable-automation')
    chrome_options.add_argument('--kiosk')  # Comment to debug
    chrome_options.add_argument('--headless')  # Comment to debug to run with UI
    chrome_options.add_argument('--incognito')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--start-maximized')  # Comment to debug
    chrome_options.add_argument('--whitelisted-ips')
    chrome_options.add_argument('--disable-infobars')
    chrome_options.add_argument('--disable-extensions')
    chrome_options.add_argument('--dns-prefetch-disable')
    chrome_options.add_argument('--disable-default-apps')
    chrome_options.add_argument('--window-size=1920,1080')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('--disable-setuid-sandbox')
    chrome_options.add_argument('--disable-popup-blocking')
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--enable-precise-memory-info')
    chrome_options.add_argument('--disable-browser-side-navigation')

    web_driver = webdriver.Chrome(executable_path=driver_path, options=chrome_options)
    web_driver.set_script_timeout(3)
    web_driver.set_page_load_timeout(10)
    return web_driver


def remove_driver(web_driver):
    if web_driver:
        web_driver.close()


def wait_for(web_driver, locator):
    return WebDriverWait(web_driver, 2).until(visibility_of_element_located(locator))


def wait_for_all(web_driver, locators):
    elements = []
    for locator in locators:
        element = WebDriverWait(web_driver, 2).until(visibility_of_element_located(locator))
        elements.append(element)
    return elements


def save_screenshot(web_driver):
    global photo_counter
    photo_counter = photo_counter + 1
    web_driver.save_screenshot(f'screenshots/screenshot{photo_counter}.png')


if __name__ == '__main__':
    try:
        run_behave('src/features --junit --junit-directory reports')
    except Exception as e:
        print(e)
