from main import create_driver, remove_driver


def before_all(context):
    """
    Called before all features
    Creates a new Selenium Web Driver and adds it to the context
    """
    context.driver = create_driver()


def after_all(context):
    """
    Called after all features
    Removes the Selenium Web Driver and adds it to the context
    """
    remove_driver(context.driver)
