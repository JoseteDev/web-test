Feature: styles feature
  # Enter feature description here


  Background: login with qa user
    When we are in path /login
    Then we set credentials qatester@emilio.com Patata100
    And click login button


  Scenario: go to home page and switch between styles
    When we are in path /
    Then switch to style Satellite
    And switch to style Light
    And switch to style Dark

    And we click on menu button
    And we click on logout button