from behave import *
from selenium.webdriver.common.by import By
from main import api_url, wait_for, save_screenshot
import time


@when('we are in path {path}')
def step_impl(context, path):
    url = api_url + path
    context.driver.get(url)
    time.sleep(1)
    save_screenshot(context.driver)
    print(f'Current url: {context.driver.current_url}')
    print(f'Url: {url}')
    assert context.driver.current_url == url


@then('we set credentials {email} {password}')
def step_impl(context, email, password):
    wait_for(context.driver, (By.ID, 'email-input')).send_keys(email)
    wait_for(context.driver, (By.ID, 'password-input')).send_keys(password)
    time.sleep(0.5)
    save_screenshot(context.driver)


@then('click login button')
def step_impl(context):
    time.sleep(0.5)
    wait_for(context.driver, (By.ID, 'login-btn')).click()
    time.sleep(0.5)


@then("we click on menu button")
def step_impl(context):
    wait_for(context.driver, (By.ID, 'menu-btn')).click()


@then("we click on admin button")
def step_impl(context):
    wait_for(context.driver, (By.ID, 'admin-btn')).click()


@then("we click on logout button")
def step_impl(context):
    wait_for(context.driver, (By.ID, 'logout-btn')).click()
