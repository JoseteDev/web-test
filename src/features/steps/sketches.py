from behave import *
from selenium.webdriver.common.by import By
from main import wait_for, save_screenshot
import time
import os


@then('we upload a sketch')
def step_impl(context):
    context.driver.find_element_by_id('file-input').send_keys(os.getcwd() + '/assets/among_us_character.zip')
    wait_for(context.driver, (By.ID, 'sketch-name')).send_keys('qa-test-sketch')
    time.sleep(0.5)
    save_screenshot(context.driver)
    wait_for(context.driver, (By.ID, 'submit')).click()
    time.sleep(2)


@then('we check the sketch')
def step_impl(context):
    wait_for(context.driver, (By.ID, 'checkbox-qa-test-sketch')).click()
    wait_for(context.driver, (By.ID, 'fly-to-qa-test-sketch')).click()
    time.sleep(1)
    save_screenshot(context.driver)


@then('we remove the sketch')
def step_impl(context):
    wait_for(context.driver, (By.ID, 'delete-qa-test-sketch')).click()
    wait_for(context.driver, (By.ID, 'confirm-btn')).click()
    time.sleep(1)
