from behave import *
from selenium.webdriver.common.by import By
from main import wait_for, save_screenshot
import time


@then('switch to style {style_name}')
def step_impl(context, style_name):
    wait_for(context.driver, (By.ID, style_name + '-checkbox')).click()
    time.sleep(1)
    save_screenshot(context.driver)
