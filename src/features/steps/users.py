from behave import *
from selenium.webdriver.common.by import By
from main import wait_for, save_screenshot
import time


@then('we click on create user button')
def step_impl(context):
    wait_for(context.driver, (By.ID, 'create-account-btn')).click()


@then('we fill the user form {username} {email} {password}')
def step_impl(context, username, email, password):
    wait_for(context.driver, (By.ID, 'username')).send_keys(username)
    wait_for(context.driver, (By.ID, 'email')).send_keys(email)
    wait_for(context.driver, (By.ID, 'password')).send_keys(password)
    wait_for(context.driver, (By.ID, 'confirm-password')).send_keys(password)
    time.sleep(0.5)
    save_screenshot(context.driver)
    wait_for(context.driver, (By.ID, 'confirm-btn')).click()
    time.sleep(1)


@then('edit the user {username} {email}')
def step_impl(context, username, email):
    search_input = wait_for(context.driver, (By.ID, 'search-input'))
    search_input.clear()
    search_input.send_keys(email)
    time.sleep(0.5)
    save_screenshot(context.driver)
    wait_for(context.driver, (By.ID, username + '-edit-btn')).click()
    wait_for(context.driver, (By.ID, 'admin-select')).click()
    wait_for(context.driver, (By.ID, 'admin-option')).click()
    time.sleep(0.5)
    save_screenshot(context.driver)
    wait_for(context.driver, (By.ID, 'confirm-btn')).click()
    time.sleep(1)


@then('delete the user {username} {email}')
def step_impl(context, username, email):
    search_input = wait_for(context.driver, (By.ID, 'search-input'))
    search_input.clear()
    search_input.send_keys(email)
    time.sleep(0.5)
    wait_for(context.driver, (By.ID, username + '-delete-btn')).click()
    time.sleep(0.5)
    save_screenshot(context.driver)
    wait_for(context.driver, (By.ID, 'confirm-btn')).click()
    time.sleep(1)
