Feature: users feature
  # Enter feature description here


  Background:
    When we are in path /login


  Scenario: create a new user and login with it
    Then we click on create user button
    And we fill the user form QA-User-Management-Test qa-user-management-test@emilio.com Patata100
    And we set credentials qa-user-management-test@emilio.com Patata100
    And click login button

    When we are in path /
    Then we click on menu button
    Then we click on logout button


  Scenario: login with qa user, edit the created user and remove it
    Then we set credentials qatester@emilio.com Patata100
    And click login button

    When we are in path /
    Then we click on menu button
    Then we click on admin button

    When we are in path /admin
    Then edit the user QA-User-Management-Test qa-user-management-test@emilio.com
    And delete the user QA-User-Management-Test qa-user-management-test@emilio.com

    And we click on menu button
    And we click on logout button