Feature: sketches feature
  # Enter feature description here


  Background: login with qa user
    When we are in path /login
    Then we set credentials qatester@emilio.com Patata100
    And click login button


  Scenario: go to home-page to upload, view and delete a sketch
    When we are in path /
    Then we upload a sketch
    And we check the sketch
    And we remove the sketch

    And we click on menu button
    And we click on logout button